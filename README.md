This repository is used for e2e checks of the
[occmd](https://gitlab.opencode.de/opencode-analyzer/occmd) tool.

All these files should be calssified as having a forbidden file type,
i.e., this is a TP/FN check.
